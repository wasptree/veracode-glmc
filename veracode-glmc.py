import os
import sys
import argparse
import gitlab
import re

parser = argparse.ArgumentParser(description='Veracode summary report to \
        Comment on Gitlab merge request')
parser.add_argument("-f", "--filename", help="The summary report to comment")
args = parser.parse_args()
filename = args.filename
if not os.path.exists(filename):
    print(" [!] Report file is not accessible. Write Veracode summary output to file")

# Grab the Gitlab private token from the environment variables

try:
    token = os.environ['GITLAB_PRIVATE_TOKEN']
except:
    print(" [!] GITLAB_PRIVATE_TOKEN environment variable is not set")
    exit(1)

project_id = os.environ['CI_PROJECT_ID']
try:
    mr_iid = os.environ['CI_MERGE_REQUEST_IID']
except:
    print(" [!] CI_MERGE_REQUEST_IID is not set. Is this a merge request?")
    exit(1)

gl = gitlab.Gitlab(private_token=token)
project = gl.projects.get(project_id)
mr = project.mergerequests.get(mr_iid)

# Read in the Veracode Summary output and reformat it for Gitlab flavour of Markdown
with open(filename, 'r') as f:
    analysis = f.read()
    analysis = "![](https://gitlab.com/wasptree/veracode-glmc/-/raw/master/images/veracode-black-hires.svg){width=300 height=100px}" + analysis
    analysis = analysis.replace('\n', '  \n')
    analysis = re.sub(r'===+', '', analysis)
    analysis = re.sub(r'---+', '\n\n---', analysis)
    analysis = re.sub(r'Scan Summary:', '## Scan Summary:', analysis)
    analysis = re.sub(r'(gitlab-ci-token:)[^@]+(.*)', r'\1<MASKED\>\2', analysis)
    analysis = re.sub(r'\nAnaly', '\n## Analy', analysis)
    analysis = re.sub(r'\nFound', '\n### Found', analysis)
    analysis = re.sub(r'\nSkipping', '\n### Skipping', analysis) 
    analysis = re.sub(r'Analyzed (\d+) issues.', r'Identified \1 Flaws.\n\n<details><summary>Details</summary>', analysis)
    analysis = re.sub(r'FAILURE: Found (\d+) issues!', r'</details>\n\n### :warning: FAILURE: \1 flaws are failing policy', analysis)
    analysis = re.sub(r'SUCCESS:', r'</details>\n\n### :white_check_mark: SUCCESS:', analysis)
    analysis = re.sub(r'CWE-(\d+)', r'[CWE-\1](https://cwe.mitre.org/data/definitions/\1.html)', analysis)

# Save note to Merge Request
mr_note = mr.notes.create({'body': analysis})
