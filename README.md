# Veracode Gitlab Merge Comment ( GLMC )
<!-- ABOUT THE PROJECT -->
## About This Project

veracode-glmc is python script to automate the process of adding a Veracode analysis summary output onto a Gitlab merge request (MR) discussion board from the CI/CD pipeline.

This is to help developers and reviewers see security analysis results directly on the merge request discussion board.

This is not an official Veracode project.


<!-- GETTING STARTED -->
## Getting Started

Docker:
A pre-built amd64 versions of the veracode-glmc image containing the dependencies and python script is available on docker hub [hub.docker.com/veracode-glmc](https://hub.docker.com/repository/docker/wasptree/veracode-glmc)

Alternatively build a local image from this repository:

  ```sh
    $ git clone https://gitlab.com/wasptree/veracode-glmc
    $ cd veracode-glmc
    $ docker build -t veracode-glmc .
  ```

Python Script:
Alternatively download and call the python script. The python-gitlab moduel is required.

<!-- SETUP -->
## Setup

For the script to be able authenticate to Gitlab and comment on the current merge request, a Gitlab private token is required.

1. Generate a valid Gitlab token for your project
   ```
   Settings -> Access Tokens -> Create Project access token
   ```
2. Set the token as an environment variable, accessible to your Gitlab pipeline. *This variable must be called GITLAB_PRIVATE_TOKEN*
   ```
   Settings -> CI/CD -> Variables -> Add Variable
   ```


<!-- USAGE EXAMPLES -->
## gitlab-ci Examples

The following 2 gitlab-ci jobs show using the Veracode pipeline scan agent to perform a SAST analysis and save a summary output to the file summary_report.txt. 

This summary report is then noted on the merge request.

Checkout the .gitlab-ci.yml file in this project.
```
pipeline_scan:
    image: openjdk:11-jre
    stage: scan
    before_script:
      - curl -sSO https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip
      - unzip pipeline-scan-LATEST.zip
    script:
      - java -jar pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file target/verademo.war -so true -sf summary_report.txt
    artifacts:
      when: always
      paths:
        - summary_report.txt
      expire_in: 1 day
    allow_failure: true
    

comment_on_merge:
    image: wasptree/veracode-glmc
    stage: report
    script:
     - python3 /opt/veracode/veracode-glmc.py --filename summary_report.txt
    allow_failure: true

```

![](/images/2023-01-05-16-29-00.png)

<!-- To Do -->
## To Do

1. Provide a method to update existing reports on the merge request
2. Add more formatting to the output
