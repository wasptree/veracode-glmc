FROM python:3.7-slim-bullseye

RUN pip install python-gitlab

RUN mkdir /opt/veracode

COPY './veracode-glmc.py' /opt/veracode/veracode-glmc.py
